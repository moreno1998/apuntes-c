CREATE DATABASE tienda

CREATE TABLE clientes (DNI, nombre, apellidos, telefono, email);
DNI char(9),
nombre varchar(20),
apellidos varchar(20),
telefono char(9),
email varchar(40),
CONSTRAINT PK (DNI);

CREATE TABLE tiendas (nombre, provincia, localidad, direccion, telefono, diapertura, diacierre, horapertura, horacierre);
nombre varchar(20),
provincia varchar(25),
localidad varchar(25),
direccion varchar(25),
telefono char(9),
diapertura enum ("lunes, martes, miercoles, jueves, viernes, sabado, domingo"),
diacierre enum ("lunes, martes, miercoles, jueves, viernes, sabado, domingo"),
horapertura time,
horacierre time,
CONSTRAINT PK (nombre);

CREATE TABLE operadoras (nombre, colorlogo, porcentajecobertura, frecuenciasgm, paginaweb);
nombre varchar(20),
colorlogo enum("rojo, azul, verde, morado, naranja"),
porcentajecobertura tinyint(3),
frecuenciasgm int(5),
paginaweb varchar(25),
CONSTRAINT PK (nombre);

CREATE TABLE tarifas (nombre, nombre_operadoras, tamañodatos, tipodatos, minutosgratis, smsgratis);
nombre varchar(20),
nombre_operadoras varchar(20),
tamañodatos tinyint unsigned,
tipodatos enum(20),
minutosgratis tinyint(3),
smsgratis tinyint(3),
CONSTRAINT PK (nombre),
CONSTRAINT FK(nombre_OPERADORAS), REFERENCES operadoras (nombre) ON UPDATE CASCADE;

CREATE TABLE moviles (marca, modelo, descripcion, so, ram, pulgadaspantalla, camarampx);
marca varchar(20),
modelo varchar(20),
descripcion varchar(20),
so varchar(20),
ram int(20),
pulgadaspantalla float(3),
camarampx float(3),
CONSTRAINT PK (marca),
CONSTRAINT PK (modelo);

CREATE TABLE movil_libre (marca_moviles, modelo_moviles, precio);
marca_moviles varchar(30),
modelo_moviles varchar(30),
precio float(42),
CONSTRAINT PK (marca_MOVILES),
CONSTRAINT PK (modelo_MOVILES),
CONSTRAINT FK (modelo_MOVILES), REFERENCES moviles (modelo) ON UPDATE CASCADE;

CREATE TABLE  movil_contrato (marca_moviles, modelo_moviles, nombre_operadoras, precio);
marca_moviles varchar(20),
modelo_moviles varchar(20),
nombre_operadoras varchar(20),
precio float(42),
CONSTRAINT PK (marca_MOVILES),
CONSTRAINT PK (modelo_MOVILES),
CONSTRAINT PK (nombre_OPERADORAS),
CONSTRAINT FK (marca_MOVILES), REFERENCES moviles (marca) ON UPDATE CASCADE,
CONSTRAINT FK (modelo_MOVILES), REFERENCES moviles (modelo) ON UPDATE CASCADE,
CONSTRAINT FK (nombre_OPERADORAS), REFERENCES operadoras (nombre) ON UPDATE CASCADE;

CREATE TABLE ofertas (nombre_OPERADORAS_TARIFAS, nombre_TARIFAS, marca_MOVILES_CONTRATO, modelo_MOVILES_CONTRATO);
nombre_OPERADORAS_TARIFAS varchar(40),
nombre_TARIFAS varchar(30),
marca_MOVILES_CONTRATO varchar(30),
modelo_MOVILES_CONTRATO varchar(30),

CONSTRAINT FK _ofertas_tarifas FOREIGN KEY (nombre_OPERADORAS_TARIFAS, nombre_TARIFAS) REFERENCES TARIFAS (nombre_OPERADORAS, nombre),

CONSTRAINT FK _ofertas_movil_contrato FOREIGN KEY (marca_MOVILES_CONTRATO, modelo_MOVILES_CONTRATO) REFERENCES movil_contrato (marca_MOVILES, modelo_MOVILES);

