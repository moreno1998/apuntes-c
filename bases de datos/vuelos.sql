DROP DATABASE if exists viajes;
CREATE DATABASE viajes DEFAULT CHARACTER SET utf8;
USE viajes;

CREATE TABLE AEROPUERTO (
CodIATA char(3),
Nombre varchar(30),
Ciudad varchar(30),
País varchar(30)
CONSTRAINT 
);

CREATE TABLE TERMINALES (
Número varchar(25),
CodIATA_AEROPUERTO char(3)
);

CREATE TABLE COMPAÑÍA (
CodCompañía smallint,
Nombre varchar(30),
Logo blob
);

CREATE TABLE VUELO (
CodVuelo smallint,
CodCompañía_COMPAÑÍA smallint,
AeropuertoOrigen varchar(30),
AeropuertoDestino varchar(30),
Estado enum('Cancelado', 'Llegando', 'Retrasado', 'Embarcando')
);

CREATE TABLE ASIENTOS (
CodAsiento char(3),
TipoClase enum('Primera clase', 'Clase Ejecutiva', 'Clase Premium Economy', 'Clase Turista')
);

CREATE TABLE PASAJEROS (
DNI char(9),
Nombre varchar(20),
Apellido1 varchar(20),
Apellido2 varchar(20)
-- CodAsiento_ASIENTOS
);

CREATE TABLE RESERVA (
Localizador char(6),
DNI_PASAJEROS char(9),
Precio decimal(6,2)
);

CREATE TABLE RESERVA_VUELOS (
Localizador_RESERVA char(6),
CodVuelo_VUELOS smallint
);