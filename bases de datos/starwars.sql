DROP DATABASE IF EXISTS starwars;
CREATE DATABASE starwars;
USE starwars;

c

CREATE TABLE personaje(
código integer PRIMARY KEY,
nombre varchar(15),
raza varchar(15),
grado varchar(20, 
códigoACTOR integer REFERENCES actores (codigo),
códigoSUPERIOR_PERSONAJE smallint,

--PRIMARY KEY (código),
--FOREIGN KEY (códigoACTOR) REFERENCES ACTOR (código) ON UPDATE CASCADE;
--FOREIGN KEY (códigoSUPERIOR_PERSONAJE) REFERENCES PERSONAJE (código);

ALTER TABLE personaje ADD FOREIGN KEY códigoSUPERIOR_PERSONAJE REFERENCES PERSONAJE (código)
);

CREATE TABLE pelicula (
código smallint,
título varchar(30),
director varchar(30),
año smallint,

PRIMARY KEY (código)
);

CREATE  TABLE personaje_pelicula (
código_PERSONAJE smallint,
código_PELICULA smallint,

FOREIGN KEY (código_PERSONAJE), REFERENCES PERSONAJE (código) ON UPDATE CASCADE,
FOREIGN KEY (código_PELICULA), REFERENCES PELICULA (código) ON UPDATE CASCADE
);

CREATE TABLE nave (
código smallint,
n_tripulantes smallint,
nombre varchar(20),

PRIMARY KEY (código)
);

CREATE TABLE visita (
código_NAVE smallint,
código_PLANETA smallint,
código_PELICULA smallint,

FOREIGN KEY (código_NAVE) REFERENCES NAVE (código) ON UPDATE CASCADE,
FOREIGN KEY (código_PLANETA) REFERENCES PLANETA (código) ON UPDATE CASCADE,
FOREIGN KEY (código_PELICULA) REFERENCES PELICULA (código) ON UPDATE CASCADE
);


ALTER TABLE personaje ADD FOREIGN KEY (codigoactor) REFERENCES actores(codigo) ON UPDATE CASCADE;

INSERT INTO actores values (1, 'Ewan Mcgregor'  '1980-01-01', 'Escocés');
INSERT INTO actores values (2, 'Natalie Portman', '1990-01-01', 'Israelí');