DROP DATABASE IF EXISTS starwars;
CREATE DATABASE starwars2;
USE starwars2;

CREATE TABLE personaje (
código integer PRIMARY KEY,
nombre varchar(15),
raza varchar(15),
grado varchar(20, 
códigoACTOR integer REFERENCES actores (codigo),
códigoSUPERIOR_PERSONAJE smallint,

);

CREATE TABLE actor(
código integer PRIMARY KEY,
nombre varchar(15),
nacionalidad varchar(25),
fecha date,


);



ALTER TABLE personaje ADD FOREIGN KEY (codigoactor) REFERENCES actor(codigo) ON UPDATE CASCADE;

INSERT INTO actor VALUES (1, 'Ewan Mcgregor'  '1980-01-01', 'Escocés');
INSERT INTO actor VALUES (2, 'Natalie Portman', '1990-01-01', 'Israelí');
INSERT INTO actor VALUES (3, 'Samuel L Jackson', '1990-01-01', 'Americana');

INSERT INTO personaje VALUES (1, 'Obi Wan Ke No Ve'. 'Humano', 'Maestro', 1);
INSERT INTO personaje VALUES (2, 'Princesa Amigdala', 'Naboo', 'Ninguno' 2);
INSERT INTO personaje VALUES (3, 'Maese Windows', 'Hunamuno', 'Maestro', 3);

UPDATE actor SET codigo=5 WHERE codigo=2;
DELETE FROM actor WHERE codigo=5;
SET FOREIGN_KEY_CHECKS=1;
-- apuntar en el UPDATE una foreing key es algo catastrófico es decir no se puede hacer de ninguna de las maneras
