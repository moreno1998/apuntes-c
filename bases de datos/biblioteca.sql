DROP DATABASE IF EXISTS biblioteca;
CREATE DATABASE biblioteca;
USE biblioteca;

CREATE TABLE socios(
nombre varchar(20) NOT NULL,
direccion varchar(30),
numero_telefono char(15),
Fecha_inscripcion date,
nºsocio int AUTO_INCREMENT,
PRIMARY KEY (nºsocio)
);

CREATE TABLE libros(
titulo varchar(30) NOT NULL,
autor varchar(20),
fecha_editado date,
nºlibros bigint AUTO_INCREMENT,
PRIMARY KEY (nºlibros)
);

CREATE TABLE prestamos(
fecha_retiro date,
fecha_entrega date,
nºsocio_socios int,
nºlibros_libros bigint,
id_prestamo bigint AUTO_INCREMENT,
PRIMARY KEY (id_prestamo)
-- FOREIGN KEY (nºsocio_socios) REFERENCES socios(nºsocio) ON UPDATE CASCADE, --
-- FOREIGN KEY (nºlibros_libros) REFERENCES libros(nºlibros) ON UPDATE CASCADE --
);

ALTER TABLE prestamos ADD CONSTRAINT prestamos_socios_FK FOREIGN KEY (nºsocio_socios) REFERENCES socios(nºsocio) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE prestamos ADD CONSTRAINT prestamos_libros_FK FOREIGN KEY (nºlibros_libros) REFERENCES libros(nºlibros) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO socios (nombre, direccion, numero_telefono, fecha_inscripcion) VALUES
('Pedro Gil', 'Canelones', 123456789, '84-12-12'),
('Jose M Foro', 'Montevideo', 987654321, '97-03-02'),
('Elba Lazo', 'Las Piedras', 666777888, '00-06-02'),
('Marta Cana', 'Montevideo', 600200200, '99-08-15');

INSERT INTO libros (titulo, autor, fecha_editado) VALUES
('El meteorologo', 'Aitor Menta', '54-12-12'),
('La fiesta', 'Encarna Vales', '87-03-02'),
('El golpe', 'Marcos Corro', '90-06-02'),
('La furia', 'Elbio Lento', '94-12-25');

INSERT INTO prestamos (nºsocio_socios, nºlibros_libros, fecha_retiro, fecha_entrega) VALUES
(1, 1, '03-12-12', '03-02-12'),
(2, 1, '03-02-02', '03-02-12'),
(2, 1, '03-02-02', '03-02-12'),
(4, 4, '03-08-03', '03-08-13');

DELETE FROM socios WHERE nºsocio=1;

UPDATE socios set nºsocio=5 WHERE nºsocio=2; 
UPDATE socios set nombre='Pepito Rodriguez' WHERE nombre='Elba Lazo';

SELECT id_prestamo AS "Orden de prestamo", nºsocio_socios, nºlibros_libros FROM prestamos LIMIT 2;

