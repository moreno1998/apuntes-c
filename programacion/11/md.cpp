#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
	int *p;  /* de esta manera declaramos un puntero */

	p = (int *) malloc ( 5 *sizeof (int)); /* el malloc para lo que nos sirve es para guardar un espacio en la memoria en este caso 4 y cuando lo utilizamos tenemos que usar el free mas el nombre del puntero en este caso p, y ahora poniendo un 5 lo que hacemos es un array de 5 */

	*p = 2;
	*(p+1) = 7; /* los arrays ya vienen con celdas reservadas, mientras que los punteros tienes que reservarlas con el malloc */
	p[2] =9; /* lo que apunta en la celda dos */

	printf ("%i - %i - %i\n", *p, p[1], *(p+2));

	free(p);


/* si por lo que sea en el malloc te quedas sin espacio habria que utilizar el realloc de la siguiente manera:  p = (int *) realloc (p, 7 * sizeof(int)); aqui diria que lo quiero agrandar 7 espacios pero si no puede lo lleva a otro sitio */

	/* que pasa si p vale null --- pues que funciona como un malloc en el caso de realloc por lo que reserva un espacio de la memoria */


return EXIT_SUCCESS;
}
